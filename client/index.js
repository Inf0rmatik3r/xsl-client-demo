function loadXMLDoc(filename) {
    if (window.ActiveXObject) {
        xhttp = new ActiveXObject('Msxml2.XMLHTTP');
    } else {
        xhttp = new XMLHttpRequest();
    }
    xhttp.open('GET', filename, false);
    try {
        xhttp.responseType = 'msxml-document';
    } catch (err) {
    } // Helping IE11
    xhttp.send('');
    return xhttp.responseXML;
}

function displayResult() {
    // Delete previous result
    document.getElementById('content').innerHTML = '';

    const name = document.getElementById('sortBy').value || 'template_descending.xsl';
    const xml = loadXMLDoc('data.xml');
    const xsl = loadXMLDoc(name);

    // code for IE
    if (window.ActiveXObject || xhttp.responseType === 'msxml-document') {
        ex = xml.transformNode(xsl);
        document.getElementById('content').innerHTML = ex;
    }

    // code for Chrome, Firefox, Opera, etc.
    else if (document.implementation && document.implementation.createDocument) {
        xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.getElementById('content').appendChild(resultDocument);
    }
}


window.addEventListener('load', () => displayResult());