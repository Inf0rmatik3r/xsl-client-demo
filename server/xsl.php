<?php

    //Pre-Load data and XSL-Stylesheet
    $xml = new DOMDocument;
    $xml->load('data.xml');

    $xsl = new DOMDocument;
    if(isset($_GET["sortBy"])) {
        $xsl->load($_GET["sortBy"]);
        
    } else {
        $xsl->load("template_unordered.xsl");
    }

    //Create new XSL Processor
    $proc = new XSLTProcessor;

    //Import Stylesheet
    $proc->importStyleSheet($xsl);

    //Transform and print converted fromat
    echo $proc->transformToXML($xml);
