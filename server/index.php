<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div>
        <form action="" method="get" id="changeForm">
            <span>Sort by: </span>
            <select name="sortBy" id="sortBy" onchange="displayResult(this)">
                <option value=""></option>
                <option value="template_descending.xsl">Highest price</option>
                <option value="template_ascending.xsl">Lowest price</option>
                <option value="template_unordered.xsl">Unordered</option>
            </select>
        </form> 
    </div>


    <?php include("xsl.php"); ?>

    <script>
        function displayResult(e) {
            document.getElementById("changeForm").submit();
        }
    </script>

</body>
</html>
