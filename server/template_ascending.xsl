<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="menu/breakfast_menu">
  <h2>Food menu</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>Name</th>
      <th>Preis</th>
      <th>Beschreibung</th>
      <th>Kalorien</th>
    </tr>
    <xsl:for-each select="food">
      <xsl:sort select="price"/>
      <tr>
          <td><xsl:value-of select="name"/></td>
          <td><xsl:value-of select="price"/></td>
          <td><xsl:value-of select="description"/></td>
          <xsl:choose>
            <xsl:when test="calories &gt; 700">
              <td style="background-color:red; color:white;"><xsl:value-of select="calories"/></td>
            </xsl:when>
            <xsl:otherwise>
              <td style="background-color:green; color:white;"><xsl:value-of select="calories"/></td>
            </xsl:otherwise>
          </xsl:choose> 
      </tr>
    </xsl:for-each>
  </table>
</xsl:template>

<xsl:template match="menu/lunch_menu">
  <h2>Lunch menu</h2>
  <ul>
  <xsl:for-each select="food">
    <li><xsl:value-of select="name"/> (<xsl:value-of select="price"/>)</li>
  </xsl:for-each>
  </ul>
</xsl:template>

</xsl:stylesheet>